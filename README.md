Sam Depardieu | Timothey Duclos | Mathéo Fournié - Groupe 4

Cahier des charges :

L’objectif principal du projet consiste à créer une application permettant la gestion simplifiée d’un complexe sportif afin d’avoir une meilleure organisation au sein du complexe. L’application pourra donc être utilisé par l’ensemble du complexe, à la fois les administrateur/l’organisation de celui-ci ainsi que les membres/adhérents du complexe en général.

Pour mieux comprendre son fonctionnement, le complexe sportif possède comme sport : Football, Handball, Volley, Badminton, Musculation.
5€ = 10 points, 10€ = 20 points, 25€ = 55 points, 50€ = 110 points,
Chaque sport est payé pour 1 mois en point :
	- Football : 10 points
	- Handball : 10 points
	- Volley : 12 points
	- Badminton : 12 points
	- Musculation : 15 points

Lors de son téléchargement l’application est gratuite et si un client veut pratiquer un sport dans ce complexe il doit créer un compte pour virer de l’argent dessus. S’il veut par exemple faire un mois de Football et de Musculation il devra se verser 25 points, soit 5€ et 10€ pour un total de 30 points soit directement 25€ pour 55 points par exemple. Les adhérents sont donc libres du montant qu’ils souhaitent ajouter ou supprimer, afin de choisir un ou plusieurs sports parmi la liste.

Fonctionnalités disponibles :

-Créer/supprimer un compte client
-Ajouter/supprimer de l’argent
-Choisir un/des sports
-Activer/désactiver un compte
-Afficher info compte client
-Afficher tous les clients
-Afficher le nombre d’adhérent par sport

Pour exécuter le programme, il suffit d'exécuter "make" dans l'invite de commande positionné dans le répertoire du programme de la salle de sport puis faire "./salledesport".

Présent sur gitlab : https://gitlab.iut-clermont.uca.fr/sadepardie1/sae-salledesport/









Répartition du travail :

Sam Depardieu : fonctions : 
	-Créer/supprimer un compte client
	-Ajouter/supprimer de l’argent
	-Afficher info compte client

Timothey Duclos : fonctions : 
	-Créer/supprimer un compte client
	-Activer/désactiver un compte
	-Afficher tous les clients

Mathéo Fournié : README, fonctions : 
	-Choisir un/des sports
	-Afficher le nombre d’adhérent par sport
