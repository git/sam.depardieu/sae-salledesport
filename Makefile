#CC : le compilateur à utiliser
CC=gcc

#CFLAGS : les options de compilation
CFLAGS= -Wall -g

# les fichiers sources : tous les fichiers présents dans src/
SRC=$(wildcard src/*.c)

# les fichiers objets (.o)
OBJ=$(patsubst src/%.c,obj/%.o,$(SRC))


#edition des liens : génération de l'exécutable à partir des .o 
executableAexecuter: $(OBJ)
	$(CC) $(OBJ) -o $@

# génération des .o à partir des .cpp et .hpp crrespondants : 
obj/%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@

#nettoyage : destruction des .o et de l'exécutable
clean:
	rm obj/*.o executableAexecuter

