#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include "salle.h"
#include <string.h>


void global(){
	int choix=1;
	char term;
	printf("\n");
	printf("\033[1;35m    ██████  ██ ███████ ███    ██ ██    ██ ███████ ███    ██ ██    ██ ███████  \033[0m\n");
	printf("\033[1;35m    ██   ██ ██ ██      ████   ██ ██    ██ ██      ████   ██ ██    ██ ██       \033[0m\n");
	printf("\033[1;35m    ██████  ██ █████   ██ ██  ██ ██    ██ █████   ██ ██  ██ ██    ██ █████    \033[0m\n");
	printf("\033[1;35m    ██   ██ ██ ██      ██  ██ ██  ██  ██  ██      ██  ██ ██ ██    ██ ██       \033[0m\n");
	printf("\033[1;35m    ██████  ██ ███████ ██   ████   ████   ███████ ██   ████  ██████  ███████  \033[0m\n");
	while (choix!=0){  
		printf("\n\n\033[1;32mQue souhaitez-vous faire?\033[0m");                                                                                                                                                    
       	printf("\n\n\033[1;36m1- Créer un adhérent\n2- Alimenter une carte\n3- Gérer l'activation d'une carte\n4- Supprimer un ahdérent\n5- Afficher les infos d'un adhérent\n6- Afficher tout les adhérents\n7- Afficher le nombre d'entrée par activité dans la journée\n8- Ajouter un ahérent dans un sport.\n0- Sortir\033[0m\n\n");
		if (scanf("%d%c",&choix,&term)!=2 || term!= '\n'){
			printf("\033[1;31mVeuillez bien tapez l'une des possibilités proposées ci-dessus.\033[0m\n");
			exit(1);
		}
		if (choix>8){
			printf("\033[1;31mVeuillez bien tapez l'une des possibilités proposées ci-dessus.\033[0m\n");
			sleep(5);
		}else if (choix==1){
			addAd();
		}else if (choix==2){
			modifPoint();
		}else if (choix==3){
			compteStatut();
		}else if (choix==4){
			suppAd();
		}else if (choix==5){
			afficheInfoAd();
		}else if (choix==6){
			afficheAd();
		}else if (choix==7){
			afficheAdSport();
		}else if (choix==8){
			ajoutSport();
		}
	}
	return;
}

void addAd (){
    FILE* fic;
    int rep,tmp,i;
    fic=fopen("numadherent.don", "r+");
    if (fic==NULL){
        printf("\033[1;31mLe fichier n'existe pas !\033[0m\n");
        exit(1);

    //Je vérifie si le fichier existe

    } 
    printf("\033[1;36mVoulez vous vraiment créer un adhérent ? \n0- Oui \n1- Non\033[0m\n");
    scanf("%d",&rep);
    if (rep==0){

        //Si l'utilisateur répond oui, on ajoute une ligne au fichier numadherent.don avec son id et en initialisant tous ses éléments.

        for (i=0; feof(fic)==0; i++){
            fscanf(fic, "%d, %d, %d, %d, %d, %d, %d, %d", &tmp, &tmp, &tmp, &tmp, &tmp, &tmp, &tmp, &tmp); //On se rend à la dernière ligne du fichier.
        }
        fprintf(fic,"%d, %d, %d, %d, %d, %d, %d, %d\n", i-1, 0, 1, 0, 0, 0, 0, 0); //On rentre l'utilisateur dans le fichier, on initialise ses éléments.
        fclose(fic);
        printf("\033[1;32mVous êtes maintenant adhérent, votre numéro id est : "); //On informe l'utilisateur qu'il est bien inscrit.
        printf("%d\n\033[0m", i-1);
        sleep(5);
    }

    //Si l'utilisatuer répond non, on ne fait rien.

    if (rep==1){
        printf("\033[1;31mVous n'avez pas crée d'adhérent\033[0m\n");
        sleep(3);
    }
}

/*
 * La fonction modifPoint permet de :
 * 		-Ajouter des points sur un compte
 * 		-Enlever des points sur un compte
*/
void modifPoint(){
	int tab[100][10] = {0};
	char c;
	int id, idtemp, nbrpnt, ptstemp, statemp, statut, foot=0, hand=0, volley=0, bad=0, muscu=0, conteur=0;
	FILE *fichier;
	fichier=fopen("numadherent.don", "r");
	printf("Quel id de compte voulez-vous modifier ? \n");
	scanf("%d", &id);
	printf("Voulez-vous enlever ou ajouter des points sur un compte ? \n \t 0 - Ajouter.\n \t 1 - Enlever.  \n");
	scanf("%d", &statut);
	// Enregistre les données du fichier existant
    while(feof(fichier)==0){
		fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &tab[conteur][0], &tab[conteur][1], &tab[conteur][2], &tab[conteur][3], &tab[conteur][4], &tab[conteur][5], &tab[conteur][6], &tab[conteur][7]);
		conteur=conteur+1;
	}
	fclose(fichier);
	fichier=fopen("numadherent.don","r+");
	// Tant que l'on arrive pas à la fin du fichier on tourne
	while((c=fgetc(fichier))!=EOF){
		fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &idtemp, &ptstemp, &statemp, &foot, &hand, &volley, &bad, &muscu);
		// Si l'id que l'on cherche est égal celui qui vient d'être scanné on continue
		if(idtemp==id){
			conteur=0;
			fclose(fichier);
			fichier=fopen("numadherent.don","w");
			// défini la taille du tableau
			while (tab[conteur][0]!=id){
				conteur=conteur+1;
			}

			if (statut == 0){ //Si on veut ajouter des points
				printf("Combien de points voulez-vous ajouter :\n");
				scanf("%d", &nbrpnt);
				if(tab[conteur][1] > 0 || tab[conteur][1] < 0){
					tab[conteur][1]+= nbrpnt;
				}
				if(tab[conteur][1] == 0){
					tab[conteur][1]= nbrpnt;
				}
			}else if (statut == 1){//Si on veut enlever des points
				printf("Combien de points voulez-vous enlever :\n");
				scanf("%d", &nbrpnt);
				if(tab[conteur][1] > 0){
					tab[conteur][1]-= nbrpnt;
				}
			}
			conteur=0;
			while(tab[conteur][0]!=0){// ajoute les données dans le fichier
				fprintf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d\n", tab[conteur][0], tab[conteur][1], tab[conteur][2], tab[conteur][3], tab[conteur][4], tab[conteur][5], tab[conteur][6], tab[conteur][7]);
				conteur=conteur+1;
			}
			fclose(fichier);
			return;
		}
    }
}

// Affiche la totalité des adhérents
void afficheAd (){
	int id, pts, statut, foot=0, hand=0, volley=0, bad=0, muscu=0;
	FILE* fichier;
	
	fichier=fopen("numadherent.don", "r");
	
	if (fichier==NULL){
		printf("Le fichier est vide");
		fclose(fichier);
		return;
	}
	
	printf("id :\tnbrpoint :\t actif :\t\tfoot :\t\thand :\t\tvolley :\tbad :\t\tmuscu :\n");
	fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &id, &pts, &statut, &foot, &hand, &volley, &bad, &muscu);
	while (feof(fichier)==0){// Parcours la totalité de la base de donnée pour l'afficher
		printf("%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\t\t%d\n", id, statut , pts, foot, hand, volley, bad, muscu);
		fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &id, &pts, &statut , &foot, &hand, &volley, &bad, &muscu);
	}
	fclose(fichier);
	
}
// Change le statut d'un compte
void compteStatut(){
	int tab[100][10] = {0};
	char c;
	int id, idtemp, ptstemp, statemp, statut, foot=0, hand=0, volley=0, bad=0, muscu=0, conteur=0;
	FILE *fichier;
	fichier=fopen("numadherent.don", "r");
	printf("Quel id de compte voulez-vous modifier ? \n");
	scanf("%d", &id);
	printf("Voulez-vous désactiver ou activer un compte ? \n \t 0 - Désactiver un compte.\n \t 1 - Activer un compte.  \n");
	scanf("%d", &statut);
	// Enregistre les données du fichier
	// Calcul la taille du fichier
    while(feof(fichier)==0){
		fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &tab[conteur][0], &tab[conteur][1], &tab[conteur][2], &tab[conteur][3], &tab[conteur][4], &tab[conteur][5], &tab[conteur][6], &tab[conteur][7]);
		conteur=conteur+1;
	}
	fclose(fichier);
	fichier=fopen("numadherent.don","r+");
	// Parcours le fichier jusquà la fin
	while((c=fgetc(fichier))!=EOF){
		fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &idtemp, &ptstemp, &statemp, &foot, &hand, &volley, &bad, &muscu);
		if(idtemp==id){
			conteur=0;
			fclose(fichier);
			fichier=fopen("numadherent.don","w"); 
			while (tab[conteur][0]!=id){
				conteur=conteur+1;
			}
			if (statut == 0){
				tab[conteur][2]=0;
			}else if (statut == 1){
				tab[conteur][2]=1;
			}
			conteur=0;
			// Enregistre les données dans le fichier
			while(tab[conteur][0]!=0){
				fprintf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d\n", tab[conteur][0], tab[conteur][1], tab[conteur][2], tab[conteur][3], tab[conteur][4], tab[conteur][5], tab[conteur][6], tab[conteur][7]);
				conteur=conteur+1;
			}
			fclose(fichier);
			return;

		}
    }
    fclose(fichier);
}

/*
 * La fonction afficheAdSport permet :
 * 		-D'afficher le nombre d'adhérents d'un sport en particulier
 *		-D'afficher le nombre d'adhérents à tous les sports
*/
void afficheAdSport() {
	int rep, rep2, id, pts, statut, foot, hand, volley, bad, muscu, f=0, h=0, v=0, b=0, m=0, t=0;
	FILE *fichier;

	fichier = fopen("numadherent.don", "r");

	if (fichier == NULL) {
		printf("Le fichier est vide");
		fclose(fichier);
		return;
	}
	printf("\033[1;36mVoulez-vous afficher le nombre d'adhérent d'un sport ? \n0- Oui \n1- Non\033[0m\n");
	scanf("%d", &rep);
	if (rep == 0) {
		while (feof(fichier) == 0) {/*Dans cette boucle on ajoute 1 à un compteur global chaque fois que l'adhérent participe à un sport et 1 à un compteur du sport concerné*/
			fscanf(fichier, "%d, %d, %d, %d, %d, %d, %d, %d", &id, &pts, &statut, &foot, &hand, &volley, &bad, &muscu);
			if (foot == 1) {
				f++;
				++t;
			}
			if (hand == 1) {
				++h;
				++t;
			}
			if (volley == 1) {
				++v;
				++t;
			}
			if (bad == 1) {
				++b;
				++t;
			}
			if (muscu == 1) {
				++m;
				++t;
			}
		}
		fclose(fichier);
		printf ("\033[1;36mQuel sport avez-vous choisi ? "
				"\n0- Football \n1- Handball \n2- Volleyball \n3- Badminton \n4- "
				"Musculation \n5- Tous\033[0m\n");
		scanf("%d", &rep2);
		if (rep2 == 0) {/*On affiche le compteur du sport en fonction de la réponse de l'utilisateur*/
			printf("Football :\t%d\n", f);
		} else if (rep2 == 1) {
			printf("Handball :\t%d\n", h);
		} else if (rep2 == 2) {
			printf("Volleball :\t%d\n", v);
		} else if (rep2 == 3) {
			printf("Badminton :\t%d\n", b);
		} else if (rep2 == 4) {
			printf("Musculation :\t%d\n", m);
		} else if (rep2 == 5) {
			printf("Tous :\t%d\n", t);
		}
	}
	if (rep == 1) {/*On affiche un message d'erreur si l'utilisateur ne souhaite pas continuer*/
		printf("\033[1;31mVous ne souhaitez pas afficher de sport\033[0m\n");
		sleep(3);
	}
	fclose(fichier);
}
// Permet d'ajouter un adhérent dans un sport
void ajoutSport(){
	int tab[100][10] = {0};
	int prix[5] = {10, 10, 12, 12, 15};
	char c;
	int id, conteur=0, sport, idtemp, ptstemp, statemp, foot, hand, volley, bad, muscu;
	FILE *fichier;

	fichier = fopen("numadherent.don", "r");
	if (fichier == NULL) {
		printf("Le fichier est vide");
		fclose(fichier);
		return;
	}
	printf("Quel est l'id de l'adhérent ? \n");
	scanf("%d", &id);
	

	// Enregistre les données du fichier 
    while(feof(fichier)==0){
		fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &tab[conteur][0], &tab[conteur][1], &tab[conteur][2], &tab[conteur][3], &tab[conteur][4], &tab[conteur][5], &tab[conteur][6], &tab[conteur][7]);
		if(tab[conteur][2] == 0){
			printf("Compte désactivé");
			return;
		}
		conteur++;
		
	}
	printf("\033[1;36m Pour quel sport adhérent à adhéré : \n0- Football \n1- Handball \n2- Volleyball \n3- Badminton \n4- Musculation \n5- Tous\033[0m\n ");
	scanf("%d", &sport);
	fclose(fichier);
	fichier=fopen("numadherent.don","r+");
	// Parcours le fichier jusqu'à la fin
	while((c=fgetc(fichier))!=EOF){
		
		fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &idtemp, &ptstemp, &statemp, &foot, &hand, &volley, &bad, &muscu);
		if(idtemp==id){
			conteur=0;
			fclose(fichier);
			fichier=fopen("numadherent.don","r+"); 
			// Calcul la taille du fichier
			while (tab[conteur][0]!=id){
				conteur=conteur+1;
			}
			if ((sport == 0 && foot == 1) || (sport == 1 && hand == 1) || (sport == 2 && volley == 1) || (sport == 3 && bad == 1) || (sport == 4 && muscu == 1)){
				printf("L'adhérent est déjà inscrit à ce sport");
			}
			else{
				// nombre de point minimum
				if(ptstemp <=-15){
					printf("L'adérent doit régler sa dette avant de adhérer à un autre sport.");
					return;
				}
				else{
					
					tab[conteur][sport+3]=1;
					tab[conteur][1] -= prix[sport];
				}
			}
			conteur=0;
			//Ajoute les données enregistré dans le fichier
			while(tab[conteur][0]!=0){
				fprintf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d\n", tab[conteur][0], tab[conteur][1], tab[conteur][2], tab[conteur][3], tab[conteur][4], tab[conteur][5], tab[conteur][6], tab[conteur][7]);
				conteur=conteur+1;
			}
			fclose(fichier);
			return;

		}
    }
}
/*
 * La fonction afficheInfoAd permet :
 * 		-Afficher les informations d'un adhérent en particulier
*/
void afficheInfoAd (){
    int iddmd, id, pts, statut, foot, hand, volley, bad, muscu;
    FILE* fichier;

    fichier=fopen("numadherent.don", "r");

    if (fichier==NULL){
        printf("Le fichier est vide");
        fclose(fichier);
        return;
    }
    printf("\033[1;36mQuel est votre id adherent ?\033[0m\n");
    scanf("%d \n", &iddmd);
    while(feof(fichier)==0){/*On parcourt le fichier*/
        fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &id, &pts, &statut , &foot, &hand, &volley, &bad, &muscu);
        if(iddmd == id){/*Lorsque l'id rentré par l'utilisateur correspond à un id dans le fichier, on affiche la ligne correspondant à l'id(les informations de l'adhérent en question)*/
            printf("id :%d\t nbrpoint :%d\t actif :%d\t\n foot :%d\t hand :%d\t volley:%d\t\n bad :%d\t muscu :%d\n", id, pts, statut, foot, hand, volley, bad, muscu);
            fclose(fichier);
            return;
        }
    }
    printf("Cet id ne correspond à aucun adherent\n");/*Si l'id rentré ne correspond à audun id dans le fichier, on affiche ce message*/
}

/* Cett fonction permet de supprimer
 * un adhérent de la base de donnée*/
void suppAd(){
	FILE* fichier;
	FILE* futurfichier;
	int tab[100][10] = {0};
	int conteur=1, asupp, numligne;
	printf("Quel adhérent voulez vous supprimer ? Veuillez entrer son id : ");
	scanf("%d", &asupp);
	fichier=fopen("numadherent.don", "r+");
	futurfichier=fopen("futurfichier.don", "w");
	fclose(futurfichier);
	futurfichier=fopen("futurfichier.don", "r+");
	
	// Enregistre les données du fichier originale
	for(numligne=1;feof(fichier)==0;++numligne){
		fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &tab[numligne][0], &tab[numligne][1], &tab[numligne][2], &tab[numligne][3], &tab[numligne][4], &tab[numligne][5], &tab[numligne][6], &tab[numligne][7]);
	}
	
	// Vérifie si l'id donné existe
	if(numligne<=asupp){
		printf("Cet utilisateur n'existe pas");
	}
	
	// Enregistre dans le nouveau fichier
	while(conteur != numligne-1){
		fscanf(fichier,"%d, %d, %d, %d, %d, %d, %d, %d", &tab[conteur][0], &tab[conteur][1], &tab[conteur][2], &tab[conteur][3], &tab[conteur][4], &tab[conteur][5], &tab[conteur][6], &tab[conteur][7]);
		// Si l'id est inférieur à l'id que l'on veut suppr on ajoute sans rien changer
		if(tab[conteur][0] < asupp) {
			fprintf(futurfichier,"%d, %d, %d, %d, %d, %d, %d, %d\n", tab[conteur][0], tab[conteur][1], tab[conteur][2], tab[conteur][3], tab[conteur][4], tab[conteur][5], tab[conteur][6], tab[conteur][7]);
		}
		// Si l'id est supérieur à l'id que l'on veut suppr on ajoute en créant un décalage
		if(tab[conteur][0] > asupp) {
			fprintf(futurfichier,"%d, %d, %d, %d, %d, %d, %d, %d\n", tab[conteur][0]-1, tab[conteur][1], tab[conteur][2], tab[conteur][3], tab[conteur][4], tab[conteur][5], tab[conteur][6], tab[conteur][7]);
		}
		conteur++;
	}
	
	fclose(fichier);
	fclose(futurfichier);
	if(remove("numadherent.don")==0){
		printf("L'utilisateur avec l'id %d a bien été supprimé", asupp);
	};
	rename("futurfichier.don","numadherent.don");
}
